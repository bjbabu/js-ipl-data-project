/* eslint-disable no-undef */
const highestPlayerOfMatchesPerSeason = require("../src/server/6-highest-player-of-macthes-per-season");

test("Highest player of matches per season with empty data", () => {
  expect(highestPlayerOfMatchesPerSeason([])).toStrictEqual({});
});

test("Highest player of matches per season with missing argument", () => {
  expect(highestPlayerOfMatchesPerSeason()).toStrictEqual({});
});

test("Passing customized input which cover multiple player with same highest to give expected output", () => {
  expect(
    highestPlayerOfMatchesPerSeason([
      {
        id: "1",
        season: "2017",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Sunrisers Hyderabad",
        team2: "Royal Challengers Bangalore",
        toss_winner: "Sunrisers Hyderabad",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Sunrisers Hyderabad",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "Yuvraj Singh",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },

      {
        id: "2",
        season: "2017",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Sunrisers Hyderabad",
        team2: "Mumbai Indians",
        toss_winner: "Mumbai Indians",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Mumbai Indians",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "Yuvraj Singh",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },

      {
        id: "3",
        season: "2017",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Gujarat Lions",
        team2: "Sunrisers Hyderabad",
        toss_winner: "Sunrisers Hyderabad",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Sunrisers Hyderabad",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "DA Warner",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },

      {
        id: "4",
        season: "2018",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Gujarat Lions",
        team2: "Mumbai Indians",
        toss_winner: "Mumbai Indians",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Gujarat Lions",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "SPD Smith",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },

      {
        id: "5",
        season: "2018",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Gujarat Lions",
        team2: "Kolkata Knight Riders",
        toss_winner: "Kolkata Knight Riders",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Kolkata Knight Riders",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "CA Lynn",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },
    ]),
  ).toStrictEqual({
    2017: {
      "Yuvraj Singh": 2,
    },
    2018: {
      "SPD Smith": 1,
      "CA Lynn": 1,
    },
  });
});
