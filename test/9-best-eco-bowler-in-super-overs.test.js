/* eslint-disable no-undef */
const bowlerStatsInSuperOvers = require("../src/server/9-best-eco-bowler-in-super-overs");

test("Highest number of times player dismissed by another player with empty data", () => {
  expect(bowlerStatsInSuperOvers([])).toStrictEqual({});
});

test("Highest number of times player dismissed by another player missing argument", () => {
  expect(bowlerStatsInSuperOvers()).toStrictEqual({});
});

test("Passing customized input to give expected output", () => {
  expect(
    bowlerStatsInSuperOvers([
      {
        ball: "1",
        batsman: "DA Warner",
        non_striker: "S Dhawan",
        bowler: "TS Mills",
        is_super_over: "0",
        wide_runs: "0",
        bye_runs: "0",
        legbye_runs: "0",
        noball_runs: "0",
        penalty_runs: "0",
        batsman_runs: "4",
        extra_runs: "0",
        total_runs: "4",
      },

      {
        ball: "2",
        batsman: "DA Warner",
        non_striker: "S Dhawan",
        bowler: "TS Mills",
        is_super_over: "1",
        wide_runs: "0",
        bye_runs: "0",
        legbye_runs: "0",
        noball_runs: "0",
        penalty_runs: "0",
        batsman_runs: "4",
        extra_runs: "0",
        total_runs: "4",
      },

      {
        ball: "3",
        batsman: "DA Warner",
        non_striker: "S Dhawan",
        bowler: "TS Mills",
        is_super_over: "1",
        wide_runs: "0",
        bye_runs: "0",
        legbye_runs: "0",
        noball_runs: "6",
        penalty_runs: "0",
        batsman_runs: "0",
        extra_runs: "6",
        total_runs: "6",
      },

      {
        ball: "4",
        batsman: "DA Warner",
        non_striker: "S Dhawan",
        bowler: "TS Mills",
        is_super_over: "1",
        wide_runs: "0",
        bye_runs: "0",
        legbye_runs: "0",
        noball_runs: "0",
        penalty_runs: "0",
        batsman_runs: "0",
        extra_runs: "0",
        total_runs: "0",
      },

      {
        ball: "1",
        batsman: "Dhoni",
        non_striker: "S Dhawan",
        bowler: "Travis Head",
        is_super_over: "1",
        wide_runs: "1",
        bye_runs: "0",
        legbye_runs: "0",
        noball_runs: "0",
        penalty_runs: "0",
        batsman_runs: "0",
        extra_runs: "1",
        total_runs: "1",
      },

      {
        ball: "2",
        batsman: "Dhoni",
        non_striker: "S Dhawan",
        bowler: "Travis Head",
        is_super_over: "1",
        wide_runs: "0",
        bye_runs: "0",
        legbye_runs: "0",
        noball_runs: "0",
        penalty_runs: "0",
        batsman_runs: "4",
        extra_runs: "0",
        total_runs: "4",
      },
    ]),
  ).toStrictEqual({
    "TS Mills": {
      given_runs: 10,
      total_balls: 2,
      economy: 30,
    },
    "Travis Head": {
      given_runs: 5,
      total_balls: 1,
      economy: 30,
    },
  });
});
