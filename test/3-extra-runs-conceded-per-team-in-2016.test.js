/* eslint-disable no-undef */
const extraRunsConcededPerTeamIn2016 = require("../src/server/3-extra-runs-conceded-per-team-in-2016");

test("Extra runs conceded per team in 2016 with empty data", () => {
  expect(extraRunsConcededPerTeamIn2016([], [], 2016)).toStrictEqual({});
});

test("Extra runs conceded per team in 2016 with missing argument", () => {
  expect(
    extraRunsConcededPerTeamIn2016(
      [
        { id: 1, season: 2016 },
        { id: 2, season: 2016 },
      ],

      2016,
    ),
  ).toStrictEqual({});
});

test(" Not passing season 2016, it should give empty object as output", () => {
  expect(
    extraRunsConcededPerTeamIn2016(
      [
        { id: 1, season: 2017 },
        { id: 2, season: 2018 },
      ],

      [
        {
          match_id: 1,
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Mumbai Indians",
          total_runs: 4,
        },
        {
          match_id: 1,
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Mumbai Indians",
          total_runs: 6,
        },
        {
          match_id: 2,
          batting_team: "Mumbai Indians",
          bowling_team: "Gujarat Lions",
          total_runs: 4,
        },
        {
          match_id: 2,
          batting_team: "Mumbai Indians",
          bowling_team: "Gujarat Lions",
          total_runs: 6,
        },
      ],

      2016,
    ),
  ).toStrictEqual({});
});

test("Passing customized input to give expected output", () => {
  expect(
    extraRunsConcededPerTeamIn2016(
      [
        { id: 1, season: 2016 },
        { id: 2, season: 2016 },
        { id: 3, season: 2017 },
      ],

      [
        {
          match_id: 1,
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Mumbai Indians",
          extra_runs: 4,
        },
        {
          match_id: 1,
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Mumbai Indians",
          extra_runs: 6,
        },
        {
          match_id: 2,
          batting_team: "Mumbai Indians",
          bowling_team: "Gujarat Lions",
          extra_runs: 14,
        },
        {
          match_id: 2,
          batting_team: "Mumbai Indians",
          bowling_team: "Gujarat Lions",
          extra_runs: 16,
        },
        {
          match_id: 3,
          batting_team: "Kings XI Punjab",
          bowling_team: "Gujarat Lions",
          extra_runs: 4,
        },
        {
          match_id: 3,
          batting_team: "Mumbai Indians",
          bowling_team: "Kings XI Punjab",
          extra_runs: 4,
        },
      ],

      2016,
    ),
  ).toStrictEqual({
    "Mumbai Indians": 10,
    "Gujarat Lions": 30,
  });
});
