/* eslint-disable no-undef */
const strikeRateOfABatsmanPerSeason = require("../src/server/7-strike-rate-of-a-batsman-per-season");

test("Strike rate of a batsman per season with empty data", () => {
  expect(strikeRateOfABatsmanPerSeason([])).toStrictEqual({});
});

test("Strike rate of a batsman per season with missing argument", () => {
  expect(strikeRateOfABatsmanPerSeason()).toStrictEqual({});
});

test("Passing customized input to give expected output", () => {
  expect(
    strikeRateOfABatsmanPerSeason(
      [
        {
          id: "1",
          season: "2017",
        },

        {
          id: "2",
          season: "2017",
        },

        {
          id: "3",
          season: "2017",
        },

        {
          id: "4",
          season: "2018",
        },

        {
          id: "5",
          season: "2019",
        },
      ],
      [
        {
          match_id: "1",
          inning: "1",
          over: "1",
          ball: "1",
          batsman: "DA Warner",
          is_super_over: "0",
          wide_runs: "1",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "0",
          extra_runs: "1",
          total_runs: "0",
        },

        {
          match_id: "1",
          inning: "1",
          over: "1",
          ball: "2",
          batsman: "DA Warner",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "6",
          extra_runs: "0",
          total_runs: "0",
        },

        {
          match_id: "2",
          inning: "1",
          over: "1",
          ball: "1",
          batsman: "DA Warner",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "4",
          extra_runs: "0",
          total_runs: "0",
        },

        {
          match_id: "1",
          inning: "1",
          over: "1",
          ball: "1",
          batsman: "Dhoni",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "2",
          extra_runs: "0",
          total_runs: "0",
        },

        {
          match_id: "1",
          inning: "1",
          over: "1",
          ball: "2",
          batsman: "Dhoni",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "4",
          extra_runs: "0",
          total_runs: "0",
        },

        {
          match_id: "1",
          inning: "1",
          over: "1",
          ball: "3",
          batsman: "Dhoni",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "6",
          penalty_runs: "0",
          batsman_runs: "0",
          extra_runs: "0",
          total_runs: "0",
        },

        {
          match_id: "2",
          inning: "1",
          over: "1",
          ball: "1",
          batsman: "Dhoni",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "6",
          extra_runs: "0",
          total_runs: "0",
        },

        {
          match_id: "4",
          inning: "1",
          over: "1",
          ball: "1",
          batsman: "Dhoni",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "2",
          extra_runs: "0",
          total_runs: "0",
        },

        {
          match_id: "4",
          inning: "1",
          over: "1",
          ball: "2",
          batsman: "Dhoni",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "4",
          extra_runs: "0",
          total_runs: "0",
        },

        {
          match_id: "5",
          inning: "1",
          over: "1",
          ball: "1",
          batsman: "Dhoni",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "3",
          extra_runs: "0",
          total_runs: "0",
        },
      ],
    ),
  ).toStrictEqual({
    "DA Warner": {
      2017: 500,
    },
    Dhoni: {
      2017: 300,
      2018: 300,
      2019: 300,
    },
  });
});
