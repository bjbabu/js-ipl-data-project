/* eslint-disable no-undef */
const matchesPerYear = require("../src/server/1-matches-per-year");

test("Mathches per year with empty data", () => {
  expect(matchesPerYear([])).toStrictEqual({});
});

test("Mathches per year with customized data", () => {
  expect(
    matchesPerYear([
      { season: 2008 },
      { season: 2009 },
      { season: 2008 },
      { season: "" },
    ]),
  ).toStrictEqual({ 2008: 2, 2009: 1 });
});
