/* eslint-disable no-undef */
const teamWonBothTossAndMatch = require("../src/server/5-times-team-won-both-toss-and-match");

test("Times team won both toss and match with empty data", () => {
  expect(teamWonBothTossAndMatch([])).toStrictEqual({});
});

test("Times team won both toss and match with missing argument", () => {
  expect(teamWonBothTossAndMatch()).toStrictEqual({});
});

test("Passing customized input to give expected output", () => {
  expect(
    teamWonBothTossAndMatch([
      {
        id: "1",
        season: "2017",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Sunrisers Hyderabad",
        team2: "Royal Challengers Bangalore",
        toss_winner: "Sunrisers Hyderabad",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Sunrisers Hyderabad",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "Yuvraj Singh",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },

      {
        id: "2",
        season: "2017",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Sunrisers Hyderabad",
        team2: "Mumbai Indians",
        toss_winner: "Mumbai Indians",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Mumbai Indians",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "Yuvraj Singh",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },

      {
        id: "3",
        season: "2017",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Gujarat Lions",
        team2: "Sunrisers Hyderabad",
        toss_winner: "Sunrisers Hyderabad",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Sunrisers Hyderabad",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "Yuvraj Singh",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },

      {
        id: "4",
        season: "2017",
        city: "Hyderabad",
        date: "2017-04-05",
        team1: "Gujarat Lions",
        team2: "Mumbai Indians",
        toss_winner: "Mumbai Indians",
        toss_decision: "field",
        result: "normal",
        dl_applied: "0",
        winner: "Gujarat Lions",
        win_by_runs: "35",
        win_by_wickets: "0",
        player_of_match: "Yuvraj Singh",
        venue: "Rajiv Gandhi International Stadium, Uppal",
        umpire1: "AY Dandekar",
        umpire2: "NJ Llong",
        umpire3: "",
      },
    ]),
  ).toStrictEqual({
    "Mumbai Indians": 1,
    "Sunrisers Hyderabad": 2,
  });
});
