/* eslint-disable no-undef */
const ecoBowlersIn2015 = require("../src/server/4-top10-eco-bowler-in-2015");

test("Top 10 eco bowlers in 2015 with empty data", () => {
  expect(ecoBowlersIn2015([], [], 2015)).toStrictEqual({});
});

test("Top 10 eco bowlers in 2015 with missing argument", () => {
  expect(
    ecoBowlersIn2015(
      [
        { id: 1, season: 2015 },
        { id: 2, season: 2015 },
      ],

      2015,
    ),
  ).toStrictEqual({});
});

test("Not passing season 2015, it should give empty object as output", () => {
  expect(
    ecoBowlersIn2015(
      [
        { id: "1", season: "2017" },
        { id: "2", season: "2018" },
      ],

      [
        {
          match_id: "3",
          inning: "1",
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Royal Challengers Bangalore",
          over: "1",
          ball: "1",
          batsman: "DA Warner",
          non_striker: "S Dhawan",
          bowler: "TS Mills",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "6",
          penalty_runs: "0",
          batsman_runs: "0",
          extra_runs: "0",
          total_runs: "0",
          player_dismissed: "",
          dismissal_kind: "",
          fielder: "",
        },

        {
          match_id: "3",
          inning: "1",
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Royal Challengers Bangalore",
          over: "1",
          ball: "1",
          batsman: "DA Warner",
          non_striker: "S Dhawan",
          bowler: "TS Mills",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "4",
          extra_runs: "0",
          total_runs: "0",
          player_dismissed: "",
          dismissal_kind: "",
          fielder: "",
        },
      ],

      2015,
    ),
  ).toStrictEqual({});
});

test("Passing customized input to give expected output", () => {
  expect(
    ecoBowlersIn2015(
      [
        { id: "1", season: "2015" },
        { id: "2", season: "2016" },
      ],

      [
        {
          match_id: "1",
          inning: "1",
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Royal Challengers Bangalore",
          over: "1",
          ball: "1",
          batsman: "DA Warner",
          non_striker: "S Dhawan",
          bowler: "TS Mills",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "6",
          penalty_runs: "0",
          batsman_runs: "0",
          extra_runs: "0",
          total_runs: "0",
          player_dismissed: "",
          dismissal_kind: "",
          fielder: "",
        },

        {
          match_id: "1",
          inning: "1",
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Royal Challengers Bangalore",
          over: "1",
          ball: "1",
          batsman: "DA Warner",
          non_striker: "S Dhawan",
          bowler: "TS Mills",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "4",
          extra_runs: "0",
          total_runs: "0",
          player_dismissed: "",
          dismissal_kind: "",
          fielder: "",
        },

        {
          match_id: "1",
          inning: "1",
          batting_team: "Sunrisers Hyderabad",
          bowling_team: "Royal Challengers Bangalore",
          over: "4",
          ball: "1",
          batsman: "MC Henriques",
          non_striker: "S Dhawan",
          bowler: "YS Chahal",
          is_super_over: "0",
          wide_runs: "0",
          bye_runs: "0",
          legbye_runs: "0",
          noball_runs: "0",
          penalty_runs: "0",
          batsman_runs: "6",
          extra_runs: "0",
          total_runs: "0",
          player_dismissed: "",
          dismissal_kind: "",
          fielder: "",
        },
      ],

      2015,
    ),
  ).toStrictEqual({
    "YS Chahal": {
      conceded_runs: 6,
      total_balls: 1,
      economy_rate: 36,
    },

    "TS Mills": {
      conceded_runs: 10,
      total_balls: 1,
      economy_rate: 60,
    },
  });
});
