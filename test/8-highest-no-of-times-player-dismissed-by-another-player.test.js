/* eslint-disable no-undef */
const highestNoOfTimesPlayerDismissedByAnotherPlayer = require("../src/server/8-highest-no-of-times-player-dismissed-by-another-player");

test("Highest number of times player dismissed by another player with empty data", () => {
  expect(highestNoOfTimesPlayerDismissedByAnotherPlayer([])).toStrictEqual({});
});

test("Highest number of times player dismissed by another player missing argument", () => {
  expect(highestNoOfTimesPlayerDismissedByAnotherPlayer()).toStrictEqual({});
});

test("Passing customized input to give expected output", () => {
  expect(
    highestNoOfTimesPlayerDismissedByAnotherPlayer([
      {
        match_id: "1",
        inning: "1",
        batsman: "DA Warner",
        non_striker: "S Dhawan",
        bowler: "TS Mills",
        player_dismissed: "DA Warner",
        dismissal_kind: "caught",
        fielder: "Mandeep Singh",
      },

      {
        match_id: "2",
        inning: "1",
        batsman: "DA Warner",
        non_striker: "S Dhawan",
        bowler: "TS Mills",
        player_dismissed: "DA Warner",
        dismissal_kind: "bowled",
        fielder: "",
      },

      {
        match_id: "3",
        inning: "1",
        batsman: "DA Warner",
        non_striker: "S Dhawan",
        bowler: "TS Mills",
        player_dismissed: "DA Warner",
        dismissal_kind: "run out",
        fielder: "BCJ Cutting",
      },

      {
        match_id: "4",
        inning: "1",
        batsman: "Yuvaraj Singh",
        non_striker: "S Dhawan",
        bowler: "Bipul Sharma",
        player_dismissed: "Yuvaraj Singh",
        dismissal_kind: "retired hurt",
        fielder: "",
      },

      {
        match_id: "5",
        inning: "1",
        batsman: "Yuvaraj Singh",
        non_striker: "S Dhawan",
        bowler: "Bipul Sharma",
        player_dismissed: "Yuvaraj Singh",
        dismissal_kind: "lbw",
        fielder: "",
      },

      {
        match_id: "6",
        inning: "1",
        batsman: "Yuvaraj Singh",
        non_striker: "S Dhawan",
        bowler: "Bipul Sharma",
        player_dismissed: "Yuvaraj Singh",
        dismissal_kind: "caught",
        fielder: "Kuldeep Yadav",
      },

      {
        match_id: "6",
        inning: "1",
        batsman: "Yuvaraj Singh",
        non_striker: "S Dhawan",
        bowler: "Bipul Sharma",
        player_dismissed: "",
        dismissal_kind: "",
        fielder: "",
      },
    ]),
  ).toStrictEqual({
    "TS Mills": {
      "DA Warner": 2,
    },

    "Bipul Sharma": {
      "Yuvaraj Singh": 2,
    },
  });
});
