/* eslint-disable no-undef */
const matchesWonPerTeamPerYear = require("../src/server/2-matches-won-per-team-per-year");

test("Mathches per year with empty data", () => {
  expect(matchesWonPerTeamPerYear([])).toStrictEqual({});
});

test("Draw matches", () => {
  expect(
    matchesWonPerTeamPerYear([{ id: 1, season: 2008, winner: "" }]),
  ).toStrictEqual({});
});

test("Mathches per year with customized data", () => {
  expect(
    matchesWonPerTeamPerYear([
      { id: 1, season: 2017, winner: "Sunrisers Hyderabad" },
      { id: 2, season: 2017, winner: "Sunrisers Hyderabad" },
      { id: 3, season: 2008, winner: "Mumbai Indians" },
      { id: 4, season: 2010, winner: "Mumbai Indians" },
    ]),
  ).toStrictEqual({
    "Sunrisers Hyderabad": {
      2017: 2,
    },
    "Mumbai Indians": {
      2008: 1,
      2010: 1,
    },
  });
});
