/* eslint-disable eqeqeq */
/* eslint-disable array-callback-return */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
function strikeRateOfABatsmanPerSeason(matches, deliveries) {
  if (
    matches === undefined ||
    deliveries === undefined ||
    matches.length === 0 ||
    deliveries.length === 0
  ) {
    return {};
  }

  // Using reduce on matches to get match IDs of every season.

  const matchIdsPerSeason = matches.reduce((matchesPlayed, match) => {
    if (!matchesPlayed[match.season]) {
      matchesPlayed[match.season] = []; // Initialized every new season with empty array.
    }

    matchesPlayed[match.season] = matchesPlayed[match.season].concat(match.id); // Concating match IDs to the respective season arrays.

    return matchesPlayed;
  }, {}); // --> { "2008": [60, 61, 62, ...... ], "2009": [118, 119, ....  ], ........, "2017": [1, 2, 3, ......] };

  // Using reduce on deliveries to get batsman statistics per each match ID.

  const batsmanStatsPerId = deliveries.reduce((batsman, delivery) => {
    if (!batsman[delivery.batsman]) {
      batsman[delivery.batsman] = {}; // Initializing new object with batsman name as Key.
    }

    if (!batsman[delivery.batsman][delivery.match_id]) {
      batsman[delivery.batsman][delivery.match_id] = { runs: 0, balls: 0 }; // Initializing for each batsman name a new object with key as match ID
      // and value is new object which stores runs and balls for each match ID.
    }

    if (delivery.wide_runs != 0) {
      batsman[delivery.batsman][delivery.match_id].runs += parseInt(
        delivery.batsman_runs,
        10,
      ); // Wide balls are exluded.
    } else {
      batsman[delivery.batsman][delivery.match_id].runs += parseInt(
        delivery.batsman_runs,
        10,
      ); // Only batsman runs are counted.
      batsman[delivery.batsman][delivery.match_id].balls++;
    }

    return batsman;
  }, {}); // --> {
  //          "DA Warner": {"1": { "runs": 14, "balls": 8}, "6" : { "runs": 76 "balls": 45 },...},
  //          "S Dhawan" : {"1": { "runs": 40, "balls": 31}, "6" : { "runs": 9 "balls": 9 },...},
  //           .......
  //      }

  // Using reduce on batsmanStatsPerId to get batsman statistics per each season.

  const batsmanStatsPerSeason = Object.entries(batsmanStatsPerId).reduce(
    (batsmanRunsAndBalls, batsman) => {
      Object.entries(matchIdsPerSeason).map((season) => {
        // Iterating through each season.

        Object.entries(batsman[1]).map((matchId) => {
          // Iterating through each batsman match IDs.

          if (season[1].includes(matchId[0])) {
            // If match Id is present in particualr season array.

            if (!batsmanRunsAndBalls[batsman[0]]) {
              batsmanRunsAndBalls[batsman[0]] = {}; // If not present, initializing with batsman name as key and a new object as value.
            }

            if (!batsmanRunsAndBalls[batsman[0]][season[0]]) {
              batsmanRunsAndBalls[batsman[0]][season[0]] = {
                total_runs: 0,
                total_balls: 0,
              }; // If not present, initializing for each batter with a season as key
              // and the value to the key is to store the total runs he scored and balls he played for each season.
            }

            batsmanRunsAndBalls[batsman[0]][season[0]].total_runs +=
              batsman[1][matchId[0]].runs;
            batsmanRunsAndBalls[batsman[0]][season[0]].total_balls +=
              batsman[1][matchId[0]].balls; // Summing up all the balls and runs played by batsmen per season.
          }
        });
      });

      return batsmanRunsAndBalls;
    },
    {},
  ); // --> {
  //          "DA Warner": {"2009": { "total_runs": 163, "total_balls": 132}, "2010" : { "total_runs": 282 "total_balls": 191 },...},
  //          "Yuvraj Singh" : {"2008": { "total_runs": 299, "total_balls": 184}, "2009" : { "total_runs": 340 "total_balls": 294 },...},
  //          ......
  //      }

  // Using reduce on batsmanStatsPerSeason to get strike rate for each season per batsman.

  const result = Object.entries(batsmanStatsPerSeason).reduce(
    (strikeRateOfBatsman, batsman) => {
      Object.entries(batsman[1]).map((season) => {
        // Iterate through each season for a batsman.

        if (!strikeRateOfBatsman[batsman[0]]) {
          strikeRateOfBatsman[batsman[0]] = {}; // If not present, initialize with batsman name as key and a new object as value.
        }

        strikeRateOfBatsman[batsman[0]][season[0]] =
          (batsman[1][season[0]].total_runs /
            batsman[1][season[0]].total_balls) *
          100; // --> strike rate = (total_runs/total_balls) * 100;
      });

      return strikeRateOfBatsman;
    },
    {},
  );

  return result;
}

module.exports = strikeRateOfABatsmanPerSeason;
