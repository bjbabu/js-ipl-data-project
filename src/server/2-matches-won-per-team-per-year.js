/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
function matchesWonPerTeamPerYear(matches) {
  const result = matches
    .filter(
      (match) =>
        // applied filter to avoid objects where winner is not declared.

        match.winner !== "",
    )
    .reduce((matchesPlayed, match) => {
      // applied reduce, which is initialize with empty object.

      // checking whether winner team name is present as key or not.

      if (matchesPlayed[match.winner]) {
        // if winner team name as key is present

        // checking whether the team key has year as key or not.

        if (matchesPlayed[match.winner][match.season]) {
          // if winner team name key has year key.

          matchesPlayed[match.winner][match.season]++; // increment one
        } else {
          // if winner team name key didnt have year key.

          matchesPlayed[match.winner][match.season] = 1; // initialize year key with value "1".
        }
      } else {
        // if winner team name as key is not present.

        matchesPlayed[match.winner] = {}; // initialzing the team as key with empty object as its value.
        matchesPlayed[match.winner][match.season] = 1;
      }

      return matchesPlayed;
    }, {});

  return result;
}

module.exports = matchesWonPerTeamPerYear;
