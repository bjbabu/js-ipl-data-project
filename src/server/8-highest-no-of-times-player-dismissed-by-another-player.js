/* eslint-disable eqeqeq */
/* eslint-disable array-callback-return */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
function highestNoOfTimesPlayerDismissedByAnotherPlayer(deliveries) {
  if (deliveries === undefined || deliveries.length === 0) {
    return {};
  }

  // Using reduce to get all dismissals for each bowler.

  const allDismisslas = deliveries.reduce((dismissals, delivery) => {
    if (delivery.player_dismissed != "") {
      // If player_dismissed is not empty.

      if (!dismissals[delivery.bowler]) {
        dismissals[delivery.bowler] = {}; // If bowler name as key is not present, initialize bowler as key with new object as value to it.
      }

      if (
        delivery.dismissal_kind != "run out" &&
        delivery.dismissal_kind != "retired hurt"
      ) {
        // If dismissal kind is strictly not equal to run out and retired hurt.

        if (!dismissals[delivery.bowler][delivery.player_dismissed]) {
          dismissals[delivery.bowler][delivery.player_dismissed] = 0; // If not present, initialize dismissed player name as key with value of 0,
          // inside the bowler name object.
        }

        dismissals[delivery.bowler][delivery.player_dismissed]++; // Increment the count of dismissed player.
      }
    }

    return dismissals;
  }, {}); // --> {
  //      "A Choudhary": { "DA Warner": 1, "PA Patel": 1, ...... },
  //      "STR Binny": { "S Dhawan": 1, "JC Buttler": 1,........ },
  //       ...............
  //     }

  // Initialized a maxDismissalCount with 0, to store max dismissal count.
  // Initialized empty object to store the result.

  let maxDismissalCount = 0;
  let highestDismissedPlayer = {};

  // Using map to iterate through allDismissals.

  Object.entries(allDismisslas).map((bowler) => {
    Object.entries(bowler[1]).map((dismissedPlayer) => {
      // Using map to iterate through each bowler inside the allDismissals.

      const currDismissalCount = dismissedPlayer[1]; // Initializing current dismissal count.

      if (currDismissalCount > maxDismissalCount) {
        // If currDismissalCount is strictly greater than maxDismissalCount.

        maxDismissalCount = currDismissalCount;

        highestDismissedPlayer = {}; // Re-initialize highestDismissed player with new object.

        highestDismissedPlayer[bowler[0]] = {};

        highestDismissedPlayer[bowler[0]][dismissedPlayer[0]] =
          maxDismissalCount; // Update the maxDismissal count to the respective bolwer name and dismissed player name.
      } else if (currDismissalCount == maxDismissalCount) {
        // If currDismissalCount is strictly equals to maxDismissalCount.

        highestDismissedPlayer[bowler[0]] = {};

        highestDismissedPlayer[bowler[0]][dismissedPlayer[0]] =
          maxDismissalCount; // Add the maxdismissal count with respective bowler name and dimissed player name/\.
      }
    });

    return highestDismissedPlayer;
  }, {});

  return highestDismissedPlayer;
}

module.exports = highestNoOfTimesPlayerDismissedByAnotherPlayer;
