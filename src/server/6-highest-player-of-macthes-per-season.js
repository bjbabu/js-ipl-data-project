/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
function highestPlayerOfMatchesPerSeason(matches) {
  if (matches === undefined || matches.length === 0) {
    return {};
  }

  // using reduce to get a count of all players of matches of all players per season.

  const playerOfMatchesPerSeason = matches.reduce((playerOfMatches, match) => {
    if (!playerOfMatches[match.season]) {
      // if season key is not present.

      playerOfMatches[match.season] = {}; // initializing season as key and assigning new object as value.
    }

    if (!playerOfMatches[match.season][match.player_of_match]) {
      // if season object doesn't have player of match name as key

      playerOfMatches[match.season][match.player_of_match] = 0; // initializing player of match name as key and assigning value 0.
    }

    playerOfMatches[match.season][match.player_of_match]++; // increments the count of player of matches per season by 1;

    return playerOfMatches;
  }, {});

  // using reduce to get the highest player of matches count of player per season.

  const result = Object.entries(playerOfMatchesPerSeason).reduce(
    (playersWonHighestPlayerOfMatchesPerSeason, season) => {
      // Oject.entries() returns an array whose elements are arrays.

      let maxCount = 0; // initializing maxCount = 0 before iterating through each season.

      // eslint-disable-next-line array-callback-return
      Object.entries(season[1]).map((players) => {
        // using the map function to iterate through each season.

        const currCount = players[1]; // initializing currCount with player of matches count of each player.

        if (currCount > maxCount) {
          // if currCount is strictly greater than maxCount.

          maxCount = currCount; // assign currCount value to maxCount.

          playersWonHighestPlayerOfMatchesPerSeason[season[0]] = {}; // have to get only the highest player per season,
          // so we have to initiate with the "season" as the key and the object as the value for the "season" key every time it satisfies the condition.

          playersWonHighestPlayerOfMatchesPerSeason[season[0]][players[0]] =
            currCount; // initiate the player name as key who has the highest player of the matches with the value of the count.
          // eslint-disable-next-line eqeqeq
        } else if (currCount == maxCount) {
          // edge case -> if multiple players have the highest player of the matches count.

          playersWonHighestPlayerOfMatchesPerSeason[season[0]][players[0]] =
            currCount; // add their name as key with value of count to the respective season.
        }
      });

      return playersWonHighestPlayerOfMatchesPerSeason;
    },
    {},
  );

  return result;
}

module.exports = highestPlayerOfMatchesPerSeason;
