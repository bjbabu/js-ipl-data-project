/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
function matchesPerYear(matches) {
  // using reduce.
  // in this, accumulator will be matchesPlayed which is initialized with empty object
  // in this, currentValue will be match

  const result = matches.reduce((matchesPlayed, match) => {
    // checking whether the year key is present or not

    if (match.season !== "") {
      if (matchesPlayed[match.season]) {
        // if the year key is present

        matchesPlayed[match.season]++; // increments the value of that key by one.
      } else {
        // if the year is not present.

        matchesPlayed[match.season] = 1; // initialzing the key with value "1";
      }
    }

    return matchesPlayed;
  }, {});

  return result;
}

module.exports = matchesPerYear; // exporting the function.
