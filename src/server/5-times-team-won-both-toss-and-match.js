/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
/* eslint-disable eqeqeq */
function teamWonBothTossAndMatch(matches) {
  if (matches == undefined || matches.length == 0) {
    return {};
  }

  // Using 'filter" function to filter the objects based on condition where match winner and toss winner are same.

  const result = matches
    .filter((match) => match.toss_winner == match.winner)

    .reduce((matchesCount, match) => {
      // using reduce to create object where team names as key and store the count of each team
      // no.of times won both toss and match.

      if (!matchesCount[match.toss_winner]) {
        // if team name as key is not present.

        matchesCount[match.toss_winner] = 1; // initialize the team name as key with value '1'.
      } else {
        // if team name as key is present.

        matchesCount[match.toss_winner]++; // increment the team name key value count by 1.
      }

      return matchesCount;
    }, {});

  return result;
}

module.exports = teamWonBothTossAndMatch;
