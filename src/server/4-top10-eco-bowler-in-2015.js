/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
function ecoBowlersIn2015(matches, deliveries, year) {
  if (
    matches.length === 0 ||
    deliveries.length === 0 ||
    matches === undefined ||
    deliveries === undefined ||
    year === undefined
  ) {
    return {};
  }

  // Using reduce for 'matches' to get array of match id's for the season 2015.

  const matchIds2015 = matches.reduce((matchIds, match) => {
    // eslint-disable-next-line eqeqeq
    if (match.season == year) {
      // checks if the season is 2015 or not.

      matchIds = matchIds.concat(match.id); // if the condition satisfies, then the match id will be conactenated to the array.
    }

    return matchIds; // -> [ "518", "519", ......"576" ]
  }, []); // Initialized with empty array.

  // Using reduce for delivers to extract object containing bowler names as key and again creating object for each bowler to store conceded runs, total balls and economy rate.

  const allBowlersEco = deliveries.reduce((allBolwers, delivery) => {
    if (matchIds2015.includes(delivery.match_id)) {
      // checks whether the delivery match id is present in the matchIds2015 array or not.

      if (!allBolwers[delivery.bowler]) {
        // checks whether the bowler name as key is present or not.

        allBolwers[delivery.bowler] = {
          conceded_runs: 0,
          total_balls: 0,
          economy_rate: 0,
        }; // if not, then initialize bowler name as key and again assigned new object to the bowler name key
        // inside the object assigned to each "bowler name" key , given 3 keys
        // conceded runs, total balls and economy rate;
      }

      // eslint-disable-next-line eqeqeq
      if (delivery.noball_runs == 0 && delivery.wide_runs == 0) {
        allBolwers[delivery.bowler].total_balls++; // total balls are incremented if and only if it is NOT a noball and NOT a wide ball.
      }

      allBolwers[delivery.bowler].conceded_runs +=
        parseInt(delivery.batsman_runs, 10) + // only batsman runs, noball runs and wide runs are added to conceded runs.
        parseInt(delivery.noball_runs, 10) +
        parseInt(delivery.wide_runs, 10);
    }

    return allBolwers; // -->  { {"UT Yadav":
    //                        {"conceded_runs": 382, "total_balls": 252, "economy_rate": 0},
    //      ......
    //     }
  }, {});

  // Using map on the previously acquired data i.e. allBowlersEco to calculate economy rate for each bowler.

  const result = Object.entries(allBowlersEco)
    .map((bowler) => {
      bowler[1].economy_rate =
        bowler[1].conceded_runs / (bowler[1].total_balls / 6); // economy rate  = ( conceded_runs ) / ( total_ball / 6 )

      return bowler; // -->  { {"UT Yadav":
      //                    {"conceded_runs": 382, "total_balls": 252, "economy_rate": 9.095238095238095},
      //      ......
      //     }
    })

    .sort((bowlerA, bowlerB) => {
      // chained with "sort" to sort the output came after applying the above map function.

      const bowlerAEco = bowlerA[1].economy_rate;
      const bowlerBEco = bowlerB[1].economy_rate;

      return bowlerAEco - bowlerBEco; // sorted in ascending order based on economy_rate.
    })

    .slice(0, 10) // slice ( start, end ) -> it will gives array with indexes 0 to 9;

    .reduce((top10EcoBowlers, bowler) => {
      // used to reduce to create new object and store the top 10 economy bowlers.

      top10EcoBowlers[bowler[0]] = {
        conceded_runs: 0,
        total_balls: 0,
        economy_rate: 0,
      };

      top10EcoBowlers[bowler[0]].conceded_runs = bowler[1].conceded_runs;
      top10EcoBowlers[bowler[0]].total_balls = bowler[1].total_balls;
      top10EcoBowlers[bowler[0]].economy_rate = bowler[1].economy_rate;

      return top10EcoBowlers;
    }, {});

  return result;
}

module.exports = ecoBowlersIn2015;
