/* eslint-disable no-param-reassign */
function extraRunsConcededPerTeamIn2016(matches, deliveries, year) {
  if (
    matches.length === 0 ||
    deliveries.length === 0 ||
    matches === undefined ||
    deliveries === undefined ||
    year === undefined
  ) {
    return {};
  }

  // Using reduce for 'matches' to get array of match id's for the season 2016.

  const matchIdsIn2016 = matches.reduce((matchIds, match) => {
    // eslint-disable-next-line eqeqeq
    if (match.season == year) {
      // checks if season equals 2016 or not.

      matchIds = matchIds.concat(match.id); // if the condition satisfies, then the match id will be conactenated to the array.
      // -> [ "577", "578", ...... ]
    }

    return matchIds;
  }, []); // initialized with empty array.

  // Using reduce for 'deliveries' to get extra runs conceded per team in year 2016. It is initialized with empty object.

  const result = deliveries.reduce((extraRunsConcededPerTeam, delivery) => {
    // iterates through deliveries.

    if (matchIdsIn2016.includes(delivery.match_id)) {
      // checks whether the delivery match id is present in the matchIds2016 array or not.

      if (!extraRunsConcededPerTeam[delivery.bowling_team]) {
        // checks whether the bowling_team name as key is present or not.

        extraRunsConcededPerTeam[delivery.bowling_team] = 0; // if not, then initialize bowling_team name as key with value : 0.
      }

      extraRunsConcededPerTeam[delivery.bowling_team] += parseInt(
        delivery.extra_runs,
        10,
      ); // if delivery match_id is present in matchIds2016 array,
      // then increment the value with extra_runs.
    }

    return extraRunsConcededPerTeam;
  }, {});

  return result;
}

module.exports = extraRunsConcededPerTeamIn2016;
