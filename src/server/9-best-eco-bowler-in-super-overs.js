/* eslint-disable array-callback-return */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
/* eslint-disable eqeqeq */
function bestEcoBowlerInSuperOvers(deliveries) {
  if (deliveries === undefined || deliveries.length === 0) {
    return {};
  }

  // Using reduce to get bowler statistics in super overs.

  const bowlerSatsInSuperOvers = deliveries.reduce((bowlerStats, delivery) => {
    if (delivery.is_super_over != 0) {
      // If it is a super over, then enters the block.

      if (!bowlerStats[delivery.bowler]) {
        // If bowler name as key is not present.

        bowlerStats[delivery.bowler] = { given_runs: 0, total_balls: 0 }; // Initialized bowler name as key with value as a new object containing
        // given_runs and total_balls as keys to store values.
      }

      if (delivery.noball_runs == 0 && delivery.wide_runs == 0) {
        bowlerStats[delivery.bowler].total_balls++; // Balls count is incremented if and only if it is NOT a noball and NOT a wide ball.
      }

      bowlerStats[delivery.bowler].given_runs +=
        parseInt(delivery.batsman_runs, 10) +
        parseInt(delivery.wide_runs, 10) +
        parseInt(delivery.noball_runs, 10); // Only batsman runs, wide runs and noball runs are contributed in total runs given.
    }

    return bowlerStats;
  }, {}); // --> {
  //      "JP Faulkner": { "given_runs": 21, "total_balls": 11, ...... },
  //      "JJ Bumrah": { "given_runs": 4, "total_balls": 6,........ },
  //       ...............
  //     }

  let minEconomy = Number.MAX_SAFE_INTEGER; // Initializing minEconomy with max integer number.
  let result = {}; // Initializing result with empty object.

  // Using map to iterate through bowlerStatsInSuperOvers to get min eco bowler.

  Object.entries(bowlerSatsInSuperOvers).map((bowler) => {
    const currEconomy = bowler[1].given_runs / (bowler[1].total_balls / 6); // Initializing currEconomy of bowler -> runs / (balls / 6)

    if (currEconomy < minEconomy) {
      // If currEconomy is strictly less than minEconomy

      minEconomy = currEconomy;

      result = {}; // Re-Initializing the result with empty object.

      result[bowler[0]] = {
        given_runs: bowler[1].given_runs,
        total_balls: bowler[1].total_balls,
        economy: minEconomy, // Updating the bowler name key with statistics and economy.
      };
    } else if (currEconomy == minEconomy) {
      // If currEconomy is equals to minEconomy

      result[bowler[0]] = {
        given_runs: bowler[1].given_runs,
        total_balls: bowler[1].total_balls,
        economy: minEconomy, // Adding the bowler name key with statistics and economy.
      };
    }
  });

  return result;
}

module.exports = bestEcoBowlerInSuperOvers;
