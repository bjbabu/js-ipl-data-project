const fs = require("fs");
const path = require("path");
const csvtojson = require("csvtojson");

const matchesCsvPath = path.join(__dirname, "./src/data/matches.csv"); // path of matches csv file.
const deliveriesCsvPath = path.join(__dirname, "./src/data/deliveries.csv"); // path of deliveries csv file.
const outputPath = path.join(__dirname, "./src/public/output"); // path where the output will be stored.

const matchesPerYear = require("./src/server/1-matches-per-year"); // imported function from src/server
const matchesWonPerTeamPerYear = require("./src/server/2-matches-won-per-team-per-year");
const extraRunsConcededPerTeamIn2016 = require("./src/server/3-extra-runs-conceded-per-team-in-2016");
const ecoBowlersIn2015 = require("./src/server/4-top10-eco-bowler-in-2015");
const teamWonBothTossAndMatch = require("./src/server/5-times-team-won-both-toss-and-match");
const highestPlayerOfMatchesPerSeason = require("./src/server/6-highest-player-of-macthes-per-season");
const strikeRateOfABatsmanPerSeason = require("./src/server/7-strike-rate-of-a-batsman-per-season");
const highestNoOfTimesPlayerDismissedByAnotherPlayer = require("./src/server/8-highest-no-of-times-player-dismissed-by-another-player");
const bestEcoBowlerInSuperOvers = require("./src/server/9-best-eco-bowler-in-super-overs");

csvtojson() // csvtojson library is used to convert csv data to json.
  .fromFile(matchesCsvPath)
  .then((matches) => {
    // console.log(matches);

    csvtojson()
      .fromFile(deliveriesCsvPath)
      .then((deliveries) => {
        // console.log(deliveries);

        /** *************************************1. Number of matches played per year for all the years in IPL.************************************** */

        let totalMatchesPerYear = matchesPerYear(matches);

        totalMatchesPerYear = JSON.stringify(totalMatchesPerYear, null, 2);

        fs.writeFile(
          path.join(outputPath, "matchesPerYear.json"),
          totalMatchesPerYear,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        /** *************************************2. Number of matches won per team per year in IPL.************************************** */

        let totalMatchesWonPerTeamPerYear = matchesWonPerTeamPerYear(matches);

        totalMatchesWonPerTeamPerYear = JSON.stringify(
          totalMatchesWonPerTeamPerYear,
          null,
          2,
        ); // JSON is stringified with space for better readability and for storing.

        // Ouput is written in specified output path using fs

        fs.writeFile(
          path.join(outputPath, "matchesWonPerTeamPerYear.json"),
          totalMatchesWonPerTeamPerYear,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        /** *************************************3. Extra runs conceded per team in the year 2016. ************************************** */

        let totalextraRunsConcededPerTeamIn2016 =
          extraRunsConcededPerTeamIn2016(matches, deliveries, 2016);

        totalextraRunsConcededPerTeamIn2016 = JSON.stringify(
          totalextraRunsConcededPerTeamIn2016,
          null,
          2,
        );

        fs.writeFile(
          path.join(outputPath, "extraRunsConcededPerTeamIn2016.json"),
          totalextraRunsConcededPerTeamIn2016,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        /** *************************************4. Top 10 economical bowlers in the year 2015. ************************************** */

        let top10EcoBowlersIn2015 = ecoBowlersIn2015(matches, deliveries, 2015);

        top10EcoBowlersIn2015 = JSON.stringify(top10EcoBowlersIn2015, null, 2);

        fs.writeFile(
          path.join(outputPath, "top10EcoBowlersIn2015.json"),
          top10EcoBowlersIn2015,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        /** *************************************5. Find the number of times each team won the toss and also won the match. ************************************** */

        let timesTeamWonBothTossAndMatch = teamWonBothTossAndMatch(matches);

        timesTeamWonBothTossAndMatch = JSON.stringify(
          timesTeamWonBothTossAndMatch,
          null,
          2,
        );

        fs.writeFile(
          path.join(outputPath, "timesTeamWonBothTossAndMatch.json"),
          timesTeamWonBothTossAndMatch,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        /** *************************************6. Find a player who has won the highest number of Player of the Match awards for each season. ************************************** */

        let playersWonHighestPlayerOfMatchesPerSeason =
          highestPlayerOfMatchesPerSeason(matches);

        playersWonHighestPlayerOfMatchesPerSeason = JSON.stringify(
          playersWonHighestPlayerOfMatchesPerSeason,
          null,
          2,
        );

        fs.writeFile(
          path.join(
            outputPath,
            "playersWonHighestPlayerOfMatchesPerSeason.json",
          ),
          playersWonHighestPlayerOfMatchesPerSeason,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        /** *************************************7. Find the strike rate of a batsman for each season. ************************************** */

        let strikeRateOfABatsmanPerSeasonResult = strikeRateOfABatsmanPerSeason(
          matches,
          deliveries,
        );

        strikeRateOfABatsmanPerSeasonResult = JSON.stringify(
          strikeRateOfABatsmanPerSeasonResult,
          null,
          2,
        );

        fs.writeFile(
          path.join(outputPath, "strikeRateOfABatsmanPerSeasonResult.json"),
          strikeRateOfABatsmanPerSeasonResult,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        /** *************************************8. Find the highest number of times one player has been dismissed by another player. ************************************** */

        let highestNoOfTimesPlayerDismissedByAnotherPlayerResult =
          highestNoOfTimesPlayerDismissedByAnotherPlayer(deliveries);
        highestNoOfTimesPlayerDismissedByAnotherPlayerResult = JSON.stringify(
          highestNoOfTimesPlayerDismissedByAnotherPlayerResult,
          null,
          2,
        );

        fs.writeFile(
          path.join(
            outputPath,
            "highestNoOfTimesPlayerDismissedByAnotherPlayerResult.json",
          ),
          highestNoOfTimesPlayerDismissedByAnotherPlayerResult,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        /** *************************************9. Find the bowler with the best economy in super overs. ************************************** */

        let bestEcoBowlerInSuperOversResult =
          bestEcoBowlerInSuperOvers(deliveries);
        bestEcoBowlerInSuperOversResult = JSON.stringify(
          bestEcoBowlerInSuperOversResult,
          null,
          2,
        );

        fs.writeFile(
          path.join(outputPath, "bestEcoBowlerInSuperOversResult.json"),
          bestEcoBowlerInSuperOversResult,
          (error) => {
            if (error) {
              console.log(error);
            }
          },
        );

        module.exports = {
          matches: this.matches,
          deliveries: this.deliveries,
        };
      });
  });
